@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Register') }}</div>

                <div class="card-body">
                    <form method="POST" action="/sujetEnregistre">
                        @csrf
                        <div class="form-group row">
                            <label for="date" type="date" class="col-md-4 col-form-label text-md-right">{{ __('Date') }}</label>

                            <div class="col-md-6">
                                <input id="date" type="date" class="form-control @error('name') is-invalid @enderror" name="date" value="{{ old('date') }}" required autocomplete="name" autofocus></div></div>
                                
                                
                                <div class="form-group row">
                            <label for="lieux" class="col-md-4 col-form-label text-md-right">{{ __('Lieux') }}</label>

                            <div class="col-md-6">
                                <input id="lieux" type="text" class="form-control @error('name') is-invalid @enderror" name="lieux" value="{{ old('lieux') }}" required autocomplete="name" autofocus></div></div>

<!--Team 1-->
<div class="form-group row">
<label for="team1" class="col-md-4 col-form-label text-md-right">{{ __('Equipe 1') }}</label>



<label for="id_user1_team1">User 1</label>
<select name="id_user1_team1" id="id_user1_team1">
    <option value="">--Please choose an user--</option>
    @foreach($users as $user)
    <option value={{$user->id}}>{{$user->name}}</option>
@endforeach
  
</select></div>
<div class="form-group row">
<label for="team1" class="col-md-4 col-form-label text-md-right"></label>

<label for="id_user2_team1">User 2</label>
<select name="id_user2_team1" id="id_user2_team1">
    <option value="">--Please choose an user--</option>
    @foreach($users as $user)
    <option value={{$user->id}}>{{$user->name}}</option>
@endforeach

  
</select></div>
  


<!--Team 2-->
<div class="form-group row">
<label for="team2" class="col-md-4 col-form-label text-md-right">{{ __('Equipe 2') }}</label>

<label for="id_user1_team2">User 1</label>
<select name="id_user1_team2" id="id_user1_team2">
    <option value="">--Please choose an user--</option>
    @foreach($users as $user)
    <option value={{$user->id}}>{{$user->name}}</option>
@endforeach

  
</select></div>

<div class="form-group row">
<label for="team2" class="col-md-4 col-form-label text-md-right"></label>
<label for="id_user2_team2">User 2</label>
<select name="id_user2_team2" id="id_user2_team2">
    <option value="">--Please choose an user--</option>
    @foreach($users as $user)
    <option value={{$user->id}}>{{$user->name}}</option>
@endforeach

  
</select></div>



                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Register') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
