@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">List des utilisateur</div>
                <div class="card-body">
                <table class="table">
                @foreach ($users as $user)
              
               
               
 
  <tbody>
    <tr>
      <th scope="row">{{$user->id}}</th>
      <td>{{$user->name}}</td>
      <td>{{$user->email}}</td>
      <td>{{implode(',' , $user->roles()->get()->pluck('name')->toArray())}}</td>
    
      <td> 
      <a href="{{route('admin.users.edit', $user->id)}}"><button class="btn btn-primary">Editer</button></a>

    
      <form action="{{ route('admin.users.destroy', $user->id) }}" method="POST" class="d-inline">
      @csrf
    
      @method('DELETE')
      <button typr="submit" class="btn btn-warning">Suprimer</button>
      </td>
    
    </tr>
 
  @endforeach
</table>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
