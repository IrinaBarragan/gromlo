<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@200;600&display=swap" rel="stylesheet">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #0000;
                color: #0000;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                weight: 80%;
                margin: auto;
            }

          

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 28px;
            }

            .links > a {
                color: #FFF;
                padding: 0 25px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }
            

          
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            @if (Route::has('login'))
                <div class="top-right links">
                    @auth
                        <a href="{{ url('/home') }}">Home</a>
                    @else
                        <a href="{{ route('login') }}">Login</a>

                        @if (Route::has('register'))
                            <a href="{{ route('register') }}">Register</a>
                        @endif
                    @endauth
                </div>
            @endif

            <div class="content">
              
                @extends('layouts.app')


@section('content')

<main>


<div class = "container bg-light pt-3 pb-3">
<div class = "d-flex row ">

  <form class ="w-100" action="/sujetEnregistre" method="GET">
      <div class = "col-sm-10 col-12 ">
          <textarea class="form-control" name="messageSujet" type="search" placeholder="textSujet" aria-label="Search"></textarea>
       </div>
       <div class = "col-sm-2 col-12 ">
          <button class=" mt-3 btn btn-outline-success bg-success text-light" name="envoiSujet"  type="submit">Envoier</button>
      </div>
      </form>
  
</div>



@endsection
</main>

</div>
            </div>
        </div>
    </body>
</html>
