<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();
Route::middleware('auth')->group(function(){
Route::get('home', 'HomeController@index')->name('home');
Route::get('sujetEnregistre', 'SujetController@saveSujet');
Route::get('allSujets', 'AllSujetController@affichSujets');
Route::get('sujetRendom', 'RendomSujetController@rendomSujet');
Route::post('sujetEnregistre', 'editBattleController@saveBattle');
Route::get('editB', 'editBattleController@index');
Route::get('battles', 'BattleController@affichBattle');

});
Route::namespace('Admin')->prefix('admin')->name('admin.')->middleware('can:manage-users');

