<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBattlesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('battles', function (Blueprint $table) {
            $table->id();
            $table->date('date');
            
            $table->string('lieux');
            $table->unsignedBigInteger('id_user1_team1')->default(1);
			$table->foreign('id_user1_team1')
				  ->references('id')
				  ->on('users')
				  ->onDelete('cascade')
				  ->onUpdate('cascade');

                  $table->unsignedBigInteger('id_user2_team1')->default(2);
                  $table->foreign('id_user2_team1')
                        ->references('id')
                        ->on('users')
                        ->onDelete('cascade')
                        ->onUpdate('cascade');
                        $table->unsignedBigInteger('id_user1_team2')->default(3);
                        $table->foreign('id_user1_team2')
                              ->references('id')
                              ->on('users')
                              ->onDelete('cascade')
                              ->onUpdate('cascade');

                              $table->unsignedBigInteger('id_user2_team2')->default(4);
                              $table->foreign('id_user2_team2')
                                    ->references('id')
                                    ->on('users')
                                    ->onDelete('cascade')
                                    ->onUpdate('cascade');
            


          
            $table->timestamps();
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('balltes', function(Blueprint $table) {
			$table->dropForeign('battles_user_id_foreign');
		});
		Schema::drop('battless');
    }
}
