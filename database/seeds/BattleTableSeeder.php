<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class PostTableSeeder extends Seeder {

    private function randDate()
	{
		return Carbon::createFromDate(null, rand(1, 12), rand(1, 28));
	}

	public function run()
	{
		DB::table('ballles')->delete();

		for($i = 0; $i < 100; ++$i)
		{
			$date = $this->randDate();
			DB::table('battles')->insert([
                'data'=>$date,
				'lieux' => 'lieux' . $i,
				
				'id_user1_team1' => rand(1, 10),
                'id_user2_team1' => rand(1, 10),
                'id_user1_team2' => rand(1, 10),
                'id_user2_team2' => rand(1, 10),
				'created_at' => $date,
				'updated_at' => $date
			]);
		}
	}
}