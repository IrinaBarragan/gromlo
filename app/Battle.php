<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Battle extends Model
{
    protected $fillable = [
        'data', 'lieux', 'id_user1_team1', 'id_user2_team1', 'id_user1_team2', 'id_user2_team2',
    ];

    public function user(){

        return $this->belongsTo(User::class);
        }
}
