<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Battle;
use App\User;

class BattleController extends Controller
{
    function affichBattle(){
        
   
        $results = DB::table('battles')->get();
        $user1;
        $user2;
        $user3;
        $user4;
        foreach ($results as $result){
            $result->user1 = json_decode(DB::table('users')->where('id', $result->id_user1_team1)->get(), true);
            $result->user2 = json_decode(DB::table('users')->where('id', $result->id_user2_team1)->get(), true);
            $result->user3 = json_decode(DB::table('users')->where('id', $result->id_user1_team2)->get(), true);
            $result->user4 = json_decode(DB::table('users')->where('id', $result->id_user2_team2)->get(), true);
 
    // $user2 = DB::table('users')->where('id', $result->id_user2_team1)->get();
    // $user3 = DB::table('users')->where('id', $result->id_user1_team2)->get();
    // $user4 = DB::table('users')->where('id', $result->id_user2_team2)->get();
        }
       
        

        $results = json_decode($results, true);
        // $user2 = json_decode($user2, true);
        // $user3 = json_decode($user3, true);
        // $user4 = json_decode($user4, true);
        
        return view('battles', ['results'=>$results]);
       
}


}