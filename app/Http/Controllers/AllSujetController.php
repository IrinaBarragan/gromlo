<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AllSujetController extends Controller
{
    function affichSujets(){
        
   
        $results = DB::table('sujets')->where('dispo', 1)->get();
        if(count($results)==0){
            return view('plusDeSujets');
        }else{
        
        
        
        $results = json_decode($results, true);
       
        
        return view('allSujets', ['results'=>$results]);
       
        }

        


}
}